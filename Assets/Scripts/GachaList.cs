using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "GachaList", menuName = "Gacha List")]
public class GachaList : ScriptableObject
{
    // This list is populated from the editor
    [SerializeField] private List<GachaReward> _items;

    // This is NonSerialized as we need it false everytime we run the game.
    // Without this tag, once set to true it will be true even after closing and restarting the game
    // Which means any future modification of our item list is not properly considered
    [System.NonSerialized] private bool isInitialized = false;

    private float _totalChance;

    private void Initialize()
    {
        if (!isInitialized)
        {
            _totalChance = _items.Sum(item => item.Chance);
            isInitialized = true;
        }
    }

    #region Alternative Initialize()
    // An alternative version that does the same operation, puts in _totalChance the sum of the chance of each item
    private void AltInitialize()
    {
        if (!isInitialized)
        {
            _totalChance = 0;
            foreach (var item in _items)
                _totalChance += item.Chance;

            isInitialized = true;
        }
    }
    #endregion

    public GachaReward GetRandomItem()
    {
        // Make sure it is initalized
        Initialize();

        // Roll our dice with _totalChance faces
        float diceRoll = Random.Range(0f, _totalChance);

        // Cycle through our items
        foreach (var item in _items)
        {
            // If item.chance is greater (or equal) than our diceRoll, we take that item and return
            if (item.Chance >= diceRoll)
                return item; // Return here, so that the cycle doesn't keep running

            // If we didn't return, we substract the chance to our diceRoll and cycle to the next item
            diceRoll -= item.Chance;
        }
        
        // As long as everything works we'll never reach this point, but better be notified if this happens!
        throw new System.Exception("Reward generation failed!");
    }
}

[System.Serializable] public class GachaReward
{
    public string Name, Type;
    public float Chance;
    public Sprite sprite;
    public int Rarity; 
    public float Health, Attack, Defense;
    public string SpecialEffect;
    public bool isOwned;
}
