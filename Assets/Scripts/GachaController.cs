using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GachaController : MonoBehaviour
{
    [SerializeField] private GameObject tableGachaPrefab, closeGachaPrefab;

    public void PopulateTable(List<TableRowItem> items)
    {
        // Populate with the new items
        items.ForEach(item =>
        {
            // Create an instance of an empty table row
            GameObject row = Instantiate(tableGachaPrefab, transform);

            // Initializes the row with our looted item data
            row.GetComponent<TableGacha>().Init(item);
        });
    }

    public void CloseResult()
    {
        // Remove old items & close gacha result
        Enumerable.Range(0, transform.childCount).ToList().ForEach(i => Destroy(transform.GetChild(i).gameObject));
        closeGachaPrefab.SetActive(false);
    }
}
