using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TableGacha : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private TMP_Text _typeText, _itemNameText, _rarityText, _amountText, _healthText, _attackText, _defenseText, _specialEffectText;
    [SerializeField] private GameObject _gameObjectDetail;

    public void Init(TableRowItem rowItem)
    {
        _typeText.text = rowItem.type;
        if (rowItem.sprite != null)
            _image.sprite = rowItem.sprite;
        _itemNameText.text = rowItem.itemName;
        _rarityText.text = "Rarity : " + rowItem.rarity.ToString() + " stars";
        _amountText.text = "x" + rowItem.amount.ToString();
        _attackText.text = "ATK : " + rowItem.attack.ToString("F0");
        _healthText.text = "HP : " + rowItem.health.ToString("F0");
        _defenseText.text = "DEF : " + rowItem.defense.ToString("F0");
        _specialEffectText.text = rowItem.specialEffect; 
    }
}

public class TableRowItem
{
    public Sprite sprite;
    public string itemName, type, specialEffect;
    public int rarity, amount;
    public float health, attack, defense;

    public TableRowItem(GachaReward gacha, int itemAmount)
    {
        type = gacha.Type;
        sprite = gacha.sprite;
        itemName = gacha.Name;
        rarity = gacha.Rarity;
        amount = itemAmount;
        health = gacha.Health;
        attack = gacha.Attack;
        defense = gacha.Defense;
        specialEffect = gacha.SpecialEffect;

        if(!gacha.isOwned)
            gacha.isOwned = true;
        
        Debug.Log(itemName + " x" + amount);
    }
}
