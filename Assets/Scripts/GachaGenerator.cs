using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GachaGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _gameObjectResult, _gameObjectContent;
    [SerializeField] private GachaController _gachaController;
    [SerializeField] private GachaList[] _gachaList;

    public void OnDrawSingle() => OnDraw(1);    //trigger button single draw
    public void OnDraw10() => OnDraw(10);       //trigger button 10 draw

    public void OnDraw(int draw)
    {
        //visible game object gacha result & set content position
        _gameObjectResult.SetActive(true);
        _gameObjectContent.transform.position = new Vector3(_gameObjectContent.transform.position.x, 0, _gameObjectContent.transform.position.z);
        
        var lootedItems = Enumerable.Range(0, draw)                                         // Read the amount of items
            .Select(_ => _gachaList[Random.Range(0, _gachaList.Length)].GetRandomItem())    // Get as many items as the read value
            .GroupBy(item => item)                                                          // Group them
            .Select(group => new TableRowItem(group.Key, group.Count()))                    // Count and create the Table Item for the simulation
            .OrderByDescending(tableRowItem => tableRowItem.rarity)                         // Sort by rarity
            .ToList();
        _gachaController.PopulateTable(lootedItems);
    }
}
